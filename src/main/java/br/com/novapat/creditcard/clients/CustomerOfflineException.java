package br.com.novapat.creditcard.clients;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.BAD_GATEWAY, reason = "O sistema de cliente está offline!")
public class CustomerOfflineException extends RuntimeException {
}
