package br.com.novapat.creditcard.clients;

import br.com.novapat.creditcard.model.dto.CustomerDTO;

public class CustomerClientFallback implements CustomerClient {

    @Override
    public CustomerDTO getCustomerById(Long id) {
           // CustomerDTO customerDTO = new CustomerDTO();
           // customerDTO.setName("Cliente default retornado quando o microserviço de cliente está fora!");
           // customerDTO.setId(1234L);
           // return customerDTO;

        throw new CustomerOfflineException();

    }

}
