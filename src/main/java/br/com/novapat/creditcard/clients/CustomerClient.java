package br.com.novapat.creditcard.clients;

import br.com.novapat.creditcard.model.dto.CustomerDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = CustomerClientConfiguration.class)
public interface CustomerClient {

    @GetMapping("v1/cliente/{id}")
    CustomerDTO getCustomerById(@PathVariable Long id);

}
