package br.com.novapat.creditcard.controller;


import br.com.novapat.creditcard.model.CreditCard;
import br.com.novapat.creditcard.model.dto.*;
import br.com.novapat.creditcard.service.CreditCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CreditCardController {

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private CreditCardMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCreditCardResponse create(@Valid @RequestBody CreateCreditCardRequest createCreditCardRequest) {
        CreditCard  creditCard = creditCardService.create(createCreditCardRequest);

        return mapper.toCreateCreditCardResponse(creditCard);
    }

    @PatchMapping("/{number}")
    public UpdateCreditCardResponse update(@PathVariable String number, @RequestBody UpdateCreditCardRequest updateCreditCardRequest) {
        updateCreditCardRequest.setNumber(number);
        CreditCard creditCard = mapper.toCreditCard(updateCreditCardRequest);

        creditCard = creditCardService.update(creditCard);

        return mapper.toUpdateCreditCardResponse(creditCard);
    }

    @GetMapping("/{number}")
    public GetCreditCardResponse getByNumber(@PathVariable String number) {
        CreditCard creditCard = creditCardService.getByNumber(number);
        return mapper.toGetCreditCardResponse(creditCard);
    }


    @GetMapping("/id/{id}")
    public GetCreditCardResponse getById(@PathVariable Long id) {
        CreditCard creditCard = creditCardService.getById(id);
        return mapper.toGetCreditCardResponse(creditCard);
    }
}
