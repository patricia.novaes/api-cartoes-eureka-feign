package br.com.novapat.creditcard;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfiguration {

    //Somente para configuração de load balancer
    @Bean
    public IRule getRule() {
        return new RandomRule();
    }

}
