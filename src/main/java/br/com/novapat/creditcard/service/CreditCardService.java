package br.com.novapat.creditcard.service;


import br.com.novapat.creditcard.clients.CustomerClient;
import br.com.novapat.creditcard.exceptions.CreditCardNotFoundException;
import br.com.novapat.creditcard.model.CreditCard;
import br.com.novapat.creditcard.model.dto.CreateCreditCardRequest;
import br.com.novapat.creditcard.model.dto.CustomerDTO;
import br.com.novapat.creditcard.repository.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreditCardService {

    @Autowired
    private CreditCardRepository creditCardRepository;

    @Autowired
    private CustomerClient customerClient;

    public CreditCard create(CreateCreditCardRequest createCreditCardRequest) {
        CustomerDTO customer = customerClient.getCustomerById(createCreditCardRequest.getCustomerId());
        CreditCard creditCard = new CreditCard();
        creditCard.setActive(false);
        creditCard.setIdCustomer(customer.getId());
        creditCard.setNumber(createCreditCardRequest.getNumber());
        return creditCardRepository.save(creditCard);
    }

    public CreditCard update(CreditCard creditCard) {
        CreditCard databaseCreditCard = getByNumber(creditCard.getNumber());

        databaseCreditCard.setActive(creditCard.getActive());

        return creditCardRepository.save(databaseCreditCard);
    }

    public CreditCard getByNumber(String number) {
        Optional<CreditCard> byId = creditCardRepository.findByNumber(number);

        if(!byId.isPresent()) {
            throw new CreditCardNotFoundException();
        }

        return byId.get();
    }

    public CreditCard getById(Long id) {
        Optional<CreditCard> byId = creditCardRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CreditCardNotFoundException();
        }

        return byId.get();
    }

}
